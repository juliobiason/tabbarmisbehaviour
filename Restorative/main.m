//
//  main.m
//  Restorative
//
//  Created by Julio Biason on 3/24/14.
//  Copyright (c) 2014 Julio Biason. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
